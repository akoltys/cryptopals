import base64
from itertools import cycle
import chunk
from builtins import len

CHARS_FREQ = {
    chr(9): 0.0057,
    chr(23): 0.0000,
    chr(32): 17.1662,
    chr(33): 0.0072,
    chr(34): 0.2442,
    chr(35): 0.0179,
    chr(36): 0.0561,
    chr(37): 0.0160,
    chr(38): 0.0226,
    chr(39): 0.2447,
    chr(40): 0.2178,
    chr(41): 0.2233,
    chr(42): 0.0628,
    chr(43): 0.0215,
    chr(44): 0.7384,
    chr(45): 1.3734,
    chr(46): 1.5124,
    chr(47): 0.1549,
    chr(48): 0.5516,
    chr(49): 0.4594,
    chr(50): 0.3322,
    chr(51): 0.1847,
    chr(52): 0.1348,
    chr(53): 0.1663,
    chr(54): 0.1153,
    chr(55): 0.1030,
    chr(56): 0.1054,
    chr(57): 0.1024,
    chr(58): 0.4354,
    chr(59): 0.1214,
    chr(60): 0.1225,
    chr(61): 0.0227,
    chr(62): 0.1242,
    chr(63): 0.1474,
    chr(64): 0.0073,
    chr(65): 0.3132,
    chr(66): 0.2163,
    chr(67): 0.3906,
    chr(68): 0.3151,
    chr(69): 0.2673,
    chr(70): 0.1416,
    chr(71): 0.1876,
    chr(72): 0.2321,
    chr(73): 0.3211,
    chr(74): 0.1726,
    chr(75): 0.0687,
    chr(76): 0.1884,
    chr(77): 0.3529,
    chr(78): 0.2085,
    chr(79): 0.1842,
    chr(80): 0.2614,
    chr(81): 0.0316,
    chr(82): 0.2519,
    chr(83): 0.4003,
    chr(84): 0.3322,
    chr(85): 0.0814,
    chr(86): 0.0892,
    chr(87): 0.2527,
    chr(88): 0.0343,
    chr(89): 0.0304,
    chr(90): 0.0076,
    chr(91): 0.0086,
    chr(92): 0.0016,
    chr(93): 0.0088,
    chr(94): 0.0003,
    chr(95): 0.1159,
    chr(96): 0.0009,
    chr(97): 5.1880,
    chr(98): 1.0195,
    chr(99): 2.1129,
    chr(100): 2.5071,
    chr(101): 8.5771,
    chr(102): 1.3725,
    chr(103): 1.5597,
    chr(104): 2.7444,
    chr(105): 4.9019,
    chr(106): 0.0867,
    chr(107): 0.6753,
    chr(108): 3.1750,
    chr(109): 1.6437,
    chr(110): 4.9701,
    chr(111): 5.7701,
    chr(112): 1.5482,
    chr(113): 0.0747,
    chr(114): 4.2586,
    chr(115): 4.3686,
    chr(116): 6.3700,
    chr(117): 2.0999,
    chr(118): 0.8462,
    chr(119): 1.3034,
    chr(120): 0.1950,
    chr(121): 1.1330,
    chr(122): 0.0596,
    chr(123): 0.0026,
    chr(124): 0.0007,
    chr(125): 0.0026,
    chr(126): 0.0003,
    chr(131): 0.0000,
    chr(149): 0.6410,
    chr(183): 0.0010,
    chr(223): 0.0000,
    chr(226): 0.0000,
    chr(229): 0.0000,
    chr(230): 0.0000,
    chr(237): 0.0000, 
}

def get_string_score(input_string):
    score = 0
    for input_char in input_string:
        input_char = chr(input_char)
        if input_char in CHARS_FREQ:
            score += CHARS_FREQ[input_char]
    return score

def find_best_key(input_string):
    return find_best_key_bytes(bytes.fromhex(input_string))

def find_best_key_bytes(input_string):
    scores = {}
    for x in range (0, 256):
        xored_input = bytes_xor_key(input_string, [x])
        scores[x] = get_string_score(xored_input)
    secret_key = max(scores, key=scores.get)
    return secret_key, scores[secret_key]


def bytes_to_base64(input_bytes):
    return base64.b64encode(input_bytes)

def bytes_from_base64(input_data):
    return base64.b64decode(input_data)

def hex_string_to_base64(input_string):
    return bytes_to_base64(bytes.fromhex(input_string))

def bytes_xor(input_bytes1, input_bytes2):
    return bytes([x ^ y for x, y in zip(input_bytes1, input_bytes2)])


def hex_string_xor(input_string1, input_string2):
    if len(input_string1) < len(input_string2):
        input_string1 += '00'*(len(input_string2) - len(input_string1))
    elif len(input_string1) > len(input_string2):
        input_string2 += '00'*(len(input_string1) - len(input_string2))
    input1 = bytes.fromhex(input_string1)
    input2 = bytes.fromhex(input_string2)
    return bytes_xor(input1, input2)


def bytes_xor_key(input_bytes, key):
    if len(input_bytes) > len(key):
        return [x ^ y for x, y in zip(input_bytes, cycle(key))]
    else:
        return [x ^ y for x, y in zip(cycle(input_bytes), key)]


def hex_string_xor_key(input_string, key):
    return bytes_xor_key(bytes.fromhex(input_string), key)


def string_xor_key(input_string, key):
    return bytes_xor_key(bytearray(input_string, 'ascii'), bytearray(key, 'ascii'))


def getHammingDistance(input1, input2):
    ''' 
    if inputs have different lengths each byte adds 8 bits of distance
    ''' 
    distance = abs(len(input1) - len(input2))*8
    for idx in range(0, min([len(input1), len(input2)])):
        #xorval = ord(input1[idx])^ord(input2[idx])
        xorval = input1[idx]^input2[idx]
        while xorval > 0:
            if xorval & 0x1:
                distance += 1
            xorval = xorval >> 1
    return distance

def getKeySize(input_data):
    results = {}
    max_chunk_size = 41
    if max_chunk_size > (len(input_data)/2):
        max_chunk_size = (len(input_data)/2)
    for i in range(2, max_chunk_size):
        results[i] = 0
        chunks = [input_data[x:x+i] for x in range(0, len(input_data), i)]
        for j in range(1, len(chunks)-2):
            results[i] += getHammingDistance(chunks[j-1], chunks[j])
        results[i] /= i*(len(chunks)-2)
    print(results)
    key_length = min(results, key=results.get)
    return key_length

def getKeyValue(input_data):
    key_size = getKeySize(input_data)
    print("Key length: ", key_size, " Data length: ", len(input_data))
    chunks = [input_data[x:x+key_size] for x in range(0, len(input_data), key_size)]
    tmpRows = {}
    for chunk in chunks:
        for i in range(0, len(chunk)):
            if i in tmpRows:
                tmpRows[i].append(chunk[i])
            else:
                tmpRows[i] = [chunk[i]]
    tmpKey = []
    for row in tmpRows:
        rKey, _ = find_best_key_bytes(bytes(tmpRows[row]))
        tmpKey.append(rKey)
    return bytes(tmpKey)

def canBeAesECB(inputData, blockSize = 16):
    if isinstance(inputData, str):
        inputData = bytearray(inputData, 'ascii')
    chunks = [inputData[idx:idx+16] for idx in range(0, len(inputData), blockSize)]
    tmp = {}
    for chunk in chunks:
        chunk = ''.join('{:02x}'.format(x) for x in chunk)
        tmp[chunk] = 1
    return len(tmp) != len(chunks)

def addPaddingPKCS7(input_data, block_len):
    if isinstance(input_data, str):
        input_data = bytearray(input_data, 'ascii')
        
    padding_value = (block_len - (len(input_data)%block_len))
    padding = []
    if padding_value > 0:
        padding = [padding_value] * padding_value
    else:
        padding = [block_len] * block_len
    return input_data + bytearray(padding)

def detectOracleBlockSize(oracle):
    test_data = b''
    empty_size = len(oracle.encrypt(test_data))
    next_size = empty_size
    while empty_size == next_size:
        test_data = test_data + b'A'
        next_size = len(oracle.encrypt(test_data))
    return (next_size-empty_size)

def detectOraclePrefixSize(oracle):
    block_size = 16
    test_data = b'A'*(block_size*2)
    while True:
        ciphered_data = oracle.encrypt(test_data)
        offset = 0
        while offset < (len(ciphered_data)-(2*block_size)):
            first_block = ciphered_data[offset:offset+block_size]
            second_block = ciphered_data[offset+block_size: offset+(2*block_size)]
            if first_block == second_block:
                padSize = len(test_data)%block_size
                return offset-padSize
            offset += 16
        test_data = test_data + b'A'
    return -1

def detectOracleECBMode(oracle):
    block_size = detectOracleBlockSize(oracle)
    test_data = bytearray([ord('A')]*block_size*3)
    return canBeAesECB(oracle.encrypt(test_data), block_size)

def urlParamsToJson(url_string):
    urlVariables = url_string.split('&')
    result = {}
    for urlVariable in urlVariables:
        tmpParams = urlVariable.split('=')
        result[tmpParams[0]] = tmpParams[1]
    return result

def stringToJson(data_string, separator_char):
    strVariables = data_string.split(separator_char)
    result = {}
    for urlVariable in strVariables:
        tmpParams = urlVariable.split('=')
        result[tmpParams[0]] = tmpParams[1]
    return result

def stripPKCS7(input_data, block_size):
    data_len = len(input_data) 
    if (data_len%block_size) != 0:
        raise Exception('Invalid padding.')
    
    pad_len = input_data[-1]
    if (pad_len > block_size) or (pad_len == data_len) or (pad_len == 0):
        raise Exception('Invalid padding.')
    
    for idx in range(2, pad_len+1):
        if (input_data[-idx] != pad_len):
            raise Exception('Invalid padding.12')
    return input_data[:-pad_len]
    