'''
Created on Nov 13, 2020

@author: akoltys
'''
import struct
from common.utils import bytes_xor

def rotate32_left(val, n):
    val = val & 0xffffffff
    return (val<<n | val >> (32-n))&0xffffffff  

def md4_funF(X, Y, Z):
    result = ((X & Y) | ((0xffffffff-X) & Z))
    return result

def md4_funG(X, Y, Z):
    result = ((X & Y) | (X & Z) | (Y & Z))
    return result

def md4_funH(X, Y, Z):
    result = (X ^ Y ^ Z)
    return result

def custom_md4(msg, additional_len = 0, initial_state = None):
    # Step 1. Append Padding Bits
    msg_len = len(msg)
    msg = msg + bytearray([0x80])
    pad_len = (56-len(msg))%64
    msg = msg + bytearray([0]*pad_len)
    # Step 2. Append Length
    msg = msg + struct.pack("<Q", msg_len*8)
    # Step 3. Initialize MD Buffer
    A = 0x67452301
    B = 0xefcdab89
    C = 0x98badcfe
    D = 0x10325476
    if (initial_state != None):
        A = int.from_bytes(initial_state[:4], byteorder='little')
        B = int.from_bytes(initial_state[4:8], byteorder='little')
        C = int.from_bytes(initial_state[8:12], byteorder='little')
        D = int.from_bytes(initial_state[12:16], byteorder='little')
    # Step 4. Process message in 16-Word Blocks
    chunks = [msg[i:i+64] for i in range(additional_len, len(msg)-63, 64)]
    for chunk in chunks:
        AA = A
        BB = B
        CC = C
        DD = D
        X = [0]*16
        for i in range(0, 16):
            X[i] = int.from_bytes(chunk[4*i:4*(i+1)], byteorder='little', signed=False)
        # Round 1.
        for k in range (0, 4):
            A = rotate32_left(A + md4_funF(B, C, D) + X[k*4+0], 3)
            D = rotate32_left(D + md4_funF(A, B, C) + X[k*4+1], 7)
            C = rotate32_left(C + md4_funF(D, A, B) + X[k*4+2], 11)
            B = rotate32_left(B + md4_funF(C, D, A) + X[k*4+3], 19)
        # Round 2.
        for k in range (0, 4):
            A = rotate32_left(A + md4_funG(B, C, D) + X[k] + 0x5A827999, 3)
            D = rotate32_left(D + md4_funG(A, B, C) + X[k+4] + 0x5A827999, 5)
            C = rotate32_left(C + md4_funG(D, A, B) + X[k+8] + 0x5A827999, 9)
            B = rotate32_left(B + md4_funG(C, D, A) + X[k+12] + 0x5A827999, 13)
        # Round 3.
        for k in [0, 2, 1, 3]:
            A = rotate32_left(A + md4_funH(B, C, D) + X[k] + 0x6ED9EBA1, 3)
            D = rotate32_left(D + md4_funH(A, B, C) + X[k+8] + 0x6ED9EBA1, 9)
            C = rotate32_left(C + md4_funH(D, A, B) + X[k+4] + 0x6ED9EBA1, 11)
            B = rotate32_left(B + md4_funH(C, D, A) + X[k+12] + 0x6ED9EBA1, 15)
        A = (A + AA)&0xffffffff
        B = (B + BB)&0xffffffff
        C = (C + CC)&0xffffffff
        D = (D + DD)&0xffffffff
    # Step 5. Output
    return (struct.pack("<L", A)+struct.pack("<L", B)+
            struct.pack("<L", C)+struct.pack("<L", D))

def custom_sha1(msg, additional_len = 0, initial_state = None):
    h0 = 0x67452301
    h1 = 0xefcdab89
    h2 = 0x98badcfe
    h3 = 0x10325476
    h4 = 0xc3d2e1f0
    if (initial_state != None):
        h0 = int.from_bytes(initial_state[:4], byteorder='big')
        h1 = int.from_bytes(initial_state[4:8], byteorder='big')
        h2 = int.from_bytes(initial_state[8:12], byteorder='big')
        h3 = int.from_bytes(initial_state[12:16], byteorder='big')
        h4 = int.from_bytes(initial_state[16:], byteorder='big')
    # print("Initial state: ", hex(h0), hex(h1), hex(h2), hex(h3), hex(h4))
    msg_len = len(msg)
    msg = msg + bytearray([0x80])
    # must append number of 0's that msg+0's+64-bit msg size is multiple of 64  
    pad_len = (56-len(msg))%64
    msg = msg + bytearray([0] * pad_len)
    # print("Msg: ", msg)
    # print("Len: ", len(msg), " AppLen: ", pad_len)
    # append bit size of the message
    msg = msg + struct.pack(">Q", msg_len*8)
    # print("Msg: ", msg)
    # print("Len: ", len(msg))
    chunks = [msg[i:i+64] for i in range(additional_len, len(msg)-63, 64)]
    for chunk in chunks:
        # print("Chunk size:", len(chunk), "Chunk: ", chunk)
        w = [0]*80
        for i in range(0, 16):
            w[i] = int.from_bytes(chunk[4*i:4*(i+1)], byteorder='big', signed=False)
            # print("W[", i,"]= ", hex(w[i]), " raw= ", chunk[4*i:4*(i+1)])
        for i in range (16, 80):
            w[i] = w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16]
            w[i] = rotate32_left(w[i], 1)
        
        a = h0
        b = h1
        c = h2
        d = h3
        e = h4
        
        for i in range(0, 80):
            if i <= 19:
                k = 0x5a827999
                tmp = rotate32_left(a, 5)+((b&c)|((~b)&d))+e+w[i]+k
                
            elif i <= 39:
                k = 0x6ed9eba1
                tmp = rotate32_left(a, 5)+(b^c^d)+e+w[i]+k
            elif i <= 59:
                k = 0x8f1bbcdc
                tmp = rotate32_left(a, 5)+((b&c)|(b&d)|(c&d))+e+w[i]+k
            else:
                k = 0xca62c1d6
                tmp = rotate32_left(a, 5)+(b^c^d)+e+w[i]+k
            tmp = tmp&0xffffffff
            e = d
            d = c
            c = rotate32_left(b, 30)
            b = a
            a = tmp
            
        h0 = (h0 + a)&0xffffffff
        h1 = (h1 + b)&0xffffffff
        h2 = (h2 + c)&0xffffffff
        h3 = (h3 + d)&0xffffffff
        h4 = (h4 + e)&0xffffffff
    # print("Final state: ", hex(h0), hex(h1), hex(h2), hex(h3), hex(h4))
    return (struct.pack(">L", h0)+struct.pack(">L", h1)+
            struct.pack(">L", h2)+struct.pack(">L", h3)+
            struct.pack(">L", h4))

def custom_hmac(key, msg, hash_fun, block_size, hash_size):
    if len(key) > block_size:
        key = hash_fun(key)
    
    if len(key) < block_size:
        key = key + bytearray([0]*(block_size-len(key)))
    
    o_key_pad = bytes_xor(key, bytearray([0x5c]*block_size))
    i_key_pad = bytes_xor(key, bytearray([0x36]*block_size))

    return hash_fun(o_key_pad + hash_fun(i_key_pad + msg))
