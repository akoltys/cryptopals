from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib
from functools import partial
from threading import Thread
from common.hash import custom_hmac
from time import sleep

HOST_ADDRESS = ""
HOST_PORT = 8000

class RequestHandler(BaseHTTPRequestHandler):
    def __init__(self, config, *args, **kwargs):
        self.M_CONFIG = config
        super().__init__(*args, **kwargs)

    def send_response(self, code, message = None):
        self.log_request(code)
        self.send_response_only(code)
        self.send_header('Server', 'python3 http.server Cryptopals Challenges Server')
        self.send_header('Date', self.date_time_string())
        self.end_headers()

    def do_GET(self):
        self.send_response(200)
        self.wfile.write(b'<head></head>')
        self.wfile.write(b'<body>')
        self.wfile.write(b'<p>Generic GET handler</p>')
        self.wfile.write(b'</body>')

    def do_POST(self):
        self.send_response(200)
        self.wfile.write(b'<head></head>')
        self.wfile.write(b'<body>')
        self.wfile.write(b'<p>Generic POST handler</p>')
        self.wfile.write(b'</body>')

class RequestHandlerHMAC50ms(RequestHandler):
    def quick_response(self, code, msg):
        self.send_response(code)
        data = b'<head></head><body>'+msg+b'</body>'
        self.wfile.write(data)


    def do_GET_test(self, query_args):
        if ('signature' not in query_args or 'file' not in query_args):
            self.quick_response(300, b'<p>Invalid request parameters</p>')
            return

        if ('hash_key' not in self.M_CONFIG or 'hash_fun' not in self.M_CONFIG or
            'hash_len' not in self.M_CONFIG or 'hash_blocklen' not in self.M_CONFIG):
            self.quick_response(500, b'<p>Invalid server configuration</p>')
            return
        hash_key = self.M_CONFIG['hash_key']
        hash_fun = self.M_CONFIG['hash_fun']
        hash_len = self.M_CONFIG['hash_len']
        hash_blocklen = self.M_CONFIG['hash_blocklen']
        user_hash = b''
        try:
            user_hash = bytes.fromhex(query_args['signature'])
        except:
            user_hash = b''
        file_data = query_args['file'].encode('utf-8')

        if len(user_hash) != hash_len:
            self.quick_response(500, b'<p>Invalid signature</p>')
            return

        valid_hash = custom_hmac(hash_key, file_data, hash_fun, hash_blocklen, hash_len)
        for idx in range(hash_len):
            if user_hash[idx] != valid_hash[idx]:
                self.quick_response(500, b'<p>Invalid signature</p>')
                return
            sleep(0.005)
        self.quick_response(200, b'<p>Signature is valid</p>')


    def do_GET(self):
        path = urllib.parse.urlparse(self.path).path
        if path == '/test':
            query = urllib.parse.urlparse(self.path).query
            query_args = dict(qc.split("=") for qc in query.split("&"))
            self.do_GET_test(query_args)
        else:
            self.send_response(200)
            self.wfile.write(b'<head></head>')
            self.wfile.write(b'<body>')
            self.wfile.write(b'<p>Unkonwn command</p>')
            self.wfile.write(b'</body>')

class HttpSrvCustom():
    M_HTTPD = None
    M_SERVER_ADDRESS = None
    M_SERVING = False
    M_DEAMON = None

    def __init__(self, handler_class = RequestHandler, config = {}):
        self.M_SERVER_ADDRESS = (HOST_ADDRESS, HOST_PORT)
        handler = partial(handler_class, config)
        self.M_HTTPD = HTTPServer(self.M_SERVER_ADDRESS, handler)
        self.M_DEAMON = Thread(name='Custom HTTP Deamon', target=self.runDaemon)
        self.M_DEAMON.setDaemon(True)

    def run(self):
        self.M_DEAMON.start()

    def join(self):
        self.M_DEAMON.join()

    def runDaemon(self):
        print("Starting server at: ", self.M_HTTPD.server_address)
        self.M_HTTPD.serve_forever()
        self.M_SERVING = True
