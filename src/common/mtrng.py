'''
Created on 14 wrz 2020

@author: akoltys
'''

class MTRng(object):
    
    def __init__(self, seed = 0):
        self._gMTidx = 0
        self._gMT = [ 0 ] * 624
        self._gMT[0] = seed
        for idx in range(1, len(self._gMT)):
            self._gMT[idx] = ((self._gMT[idx-1]>>30)^self._gMT[idx-1]*
                              0x6c078965 + 1) & 0xffffffff
    
    def updateState(self, state):
        self._gMT = state
    
    def extractNumber(self):
        if self._gMTidx == 0:
            self._generateNumbers()
        
        tmp = self._gMT[self._gMTidx]
        tmp = (tmp ^ (tmp>>11))&0xffffffff
        tmp = (tmp ^ ((tmp<<7) & 0x9d2c5680))&0xffffffff
        tmp = (tmp ^ ((tmp<<15) & 0xefc60000))&0xffffffff
        tmp = (tmp ^ (tmp>>18))&0xffffffff
        self._gMTidx = (self._gMTidx+1)%len(self._gMT)
        return tmp
    
    def _generateNumbers(self):
        for idx in range(len(self._gMT)):
            tmp = self._gMT[idx]&0x80000000
            tmp |= self._gMT[(idx+1)%len(self._gMT)]&0x7fffffff
            self._gMT[idx] = self._gMT[(idx+397)%len(self._gMT)] ^ (tmp>>1)
            if tmp&0x1 != 0:
                self._gMT[idx] = self._gMT[idx] ^ 0x9908b0df

def _invertValue(value):
    result = 0;
    for _ in range(32):
        result = (result<<1) | (value&0x1)
        value = value>>1
    return result

def _revertXorShifMask(value, shift, mask):
    """shift > 0 - shl, shift < 0 shr"""
    result = 0
    shiftC = shift
    if shift < 0:
        value = _invertValue(value)
        shiftC = abs(shift)
    
    for idx in range(32):
        if idx < shiftC:
            result = result | (value & (0x1<<idx))
        else:
            tmp = (value & (0x1<<idx))
            xorBit = result&(0x1<<(idx-shiftC))
            xorBit = xorBit << shiftC
            maskBit = (mask&(0x1<<idx))
            tmp = tmp ^ (xorBit&maskBit)
            result = result | tmp

    if (shift < 0):
        result = _invertValue(result)
    return result

def untemperNumber(value):
    tmp = value
    tmp = _revertXorShifMask(tmp, -18, 0xffffffff)
    tmp = _revertXorShifMask(tmp, 15, 0xefc60000)
    tmp = _revertXorShifMask(tmp, 7, 0x9d2c5680)
    tmp = _revertXorShifMask(tmp, -11, 0xffffffff)
    return tmp