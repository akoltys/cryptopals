from common.utils import hex_string_to_base64, hex_string_xor,\
    string_xor_key, find_best_key, bytes_from_base64,\
    getKeyValue, bytes_xor_key, canBeAesECB
from common.aes import CustomAES


def s1challenge1():
    print("Challenge 1 start")
    input_string = ("49276d206b696c6c696e6720796f757220627261696e206c696b65206"
                    "120706f69736f6e6f7573206d757368726f6f6d")
    expected_output = b'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t'
    output_string = hex_string_to_base64(input_string)
    print(output_string)
    print(expected_output)


def s1challenge2():
    print("Challenge 2 start")
    input_string1 = "1c0111001f010100061a024b53535009181c"
    input_string2 = "686974207468652062756c6c277320657965"
    output = hex_string_xor(input_string1, input_string2)
    print(output.hex())
    print("746865206b696420646f6e277420706c6179")
    output = hex_string_xor("0011", "220033")
    print(output.hex())


def s1challenge3():
    print("Challenge 3 start")
    input_string = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    tmp_key, tmp_score = find_best_key(input_string)
    decoded_msg = ''.join([chr(x) for x in bytes_xor_key(bytes.fromhex(input_string), [tmp_key])])
    print((decoded_msg), tmp_score)


def s1challenge4():
    print("Challenge 4 start")
    result = ""
    score = 0
    with open('testfiles/4.txt') as f:
        file_lines = f.readlines()
        for line in file_lines:
            if line[-1] == '\n':
                line = line[:-1]
            if line[-1] == '\n':
                line = line[:-1]
            new_key, new_score = find_best_key(line)
            if new_score > score:
                score = new_score
                secret_msg = bytes_xor_key(bytes.fromhex(line), [new_key])
                result = ''.join([chr(x) for x in secret_msg])
    print(result)


def s1challenge5():
    print("Challenge 5 start")
    test_stirng1 = ("Burning 'em, if you ain't quick and nimble\n"
                    "I go crazy when I hear a cymbal")
    expected_string1 = ("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d633"
                        "43c2a26226324272765272a282b2f20430a652e2c652a3124333a"
                        "653e2b2027630c692b20283165286326302e27282f")
    result_string1 = string_xor_key(test_stirng1, "ICE")
    print('1. Got:\t\t', ''.join(format(x, '02x') for x in result_string1), 
          "\n2. Expected:\t", expected_string1)


def s1challenge6():
    print("Challenge 6 start")
    with open('testfiles/6.txt') as f:
        file_string_b64 = f.read().replace('\n', '')
        print(file_string_b64);
        file_data = bytes_from_base64(file_string_b64)
        print(file_data)
        secret_key = getKeyValue(file_data)
        print("Secret key: ", secret_key)
        print("Secret message: ")
        print(''.join([chr(x) for x in bytes_xor_key(file_data, secret_key)]))
        

def s1challenge7():
    print("Challenge 7 start")
    with open('testfiles/7.txt') as f:
        file_string_b64 = f.read().replace('\n', '')
        print(file_string_b64);
        file_data = bytes_from_base64(file_string_b64)
#         secret_key = bytes('YELLOW SUBMARINE', 'ascii')
#         cipher = AES.new(secret_key, AES.MODE_ECB)
#         plain_data = cipher.decrypt(file_data)
        cipher = CustomAES()
        plain_data = cipher.decode(file_data, 'YELLOW SUBMARINE')
        print(plain_data)


def s1challenge8():
    print("Challenge 8 start")
    with open('testfiles/8.txt') as f:
        lines = f.readlines();
        line_nbr = 0
        for line in lines:
            line_nbr += 1
            if canBeAesECB(line[:-2]):
                print("Probably encoded with ECB on line ", line_nbr, ": ", line)
