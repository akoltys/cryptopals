'''
Created on 17 kwi 2020

@author: akoltys
'''

from common.utils import addPaddingPKCS7, bytes_from_base64, canBeAesECB,\
    detectOracleBlockSize, detectOracleECBMode,\
    detectOraclePrefixSize, stripPKCS7
from common.aes import CustomAES
from common import oracles
from common.oracles import OracleAesECB, OracleProfileFor, OracleAesECBHarder,\
    OracleCbcBitFlipping

def s2challenge9():
    print("Challenge 9")
    test_data = "YELLOW SUBMARINE"
    print("Result: ", addPaddingPKCS7(test_data, 20))

def s2challenge10():
    print("Challenge 10 start")
    with open('testfiles/10.txt') as f:
        file_string_b64 = f.read().replace('\n', '')
        print(file_string_b64);
        file_data = bytes_from_base64(file_string_b64)
        cipher = CustomAES()
        plain_data = cipher.decode(file_data, 'YELLOW SUBMARINE',
                                   CustomAES.Mode.CBC,
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        print(plain_data)

def s2challenge11():
    print("Challenge 11 start")
    for i in range(0, 10):
        ciphered_data, cipher_type = oracles.encryptionOracle("a"*48)
        print(i+1, ". Cipher type: ", cipher_type == CustomAES.Mode.ECB,
              " Guessed Type", canBeAesECB(ciphered_data))

def s2challenge12():
    print("Challenge 12 start")
    secret_base64 = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg" \
                    "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq" \
                    "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg" \
                    "YnkK"
    oracle = OracleAesECB(secret_base64)
    detected_block_size = detectOracleBlockSize(oracle)
    detected_ecb_mode = detectOracleECBMode(oracle)
    print("Secret size: ", len(secret_base64)*3/4)
    print("Detected block size: ", detected_block_size)
    print("Detected ECB mode: ", detected_ecb_mode)
    block_num = 0
    deciphered_message = []
    previous_block = [ord('A')]*(detected_block_size)
    decyphering_running = True
    while decyphering_running:
        deciphered_block = []
        pad_size = 1
        while pad_size <= detected_block_size:
            test_block = previous_block[pad_size:]
            ciphered_block = oracle.encrypt(bytearray(test_block))
            test_val = 0x0
            tmp_block = test_block+deciphered_block+[test_val]
            tmp_ciphered_block = oracle.encrypt(bytearray(tmp_block))
            block_offset = block_num*detected_block_size
            oldBlock = ciphered_block[block_offset:block_offset+detected_block_size]
            newBlock = tmp_ciphered_block[0:detected_block_size]
            while newBlock != oldBlock and test_val < 255:
                test_val = test_val + 1
                tmp_block = test_block+deciphered_block+[test_val]
                tmp_ciphered_block = oracle.encrypt(bytearray(tmp_block))
                newBlock = tmp_ciphered_block[0:detected_block_size]
            deciphered_block += [test_val]
            pad_size += 1
        previous_block = deciphered_block
        deciphered_message += deciphered_block
        block_num += 1
        if block_num >= len(ciphered_block)/detected_block_size:
            decyphering_running = False
    print(bytearray(deciphered_message))

def s2challenge13():
    print("Challenge 13 start")
    oracle = OracleProfileFor()
    encProf = oracle.getProfile("aaaaaaaaaaadmin'\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0a\x0aaaaaauser@gmail.com")
    decProf = oracle.decodeProfile(encProf[:16]+encProf[32:-16]+encProf[16:32])
    print(decProf, len(decProf))

def s2challenge14():
    print("Challenge 14 start")
    secret_base64 = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg" \
                    "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq" \
                    "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg" \
                    "YnkK"
    oracle = OracleAesECBHarder(secret_base64)
    detected_block_size = detectOracleBlockSize(oracle)
    detected_ecb_mode = detectOracleECBMode(oracle)
    detected_prefix_size = detectOraclePrefixSize(oracle)
    detected_prefix_pad_size = detected_block_size - (detected_prefix_size%detected_block_size)
    detected_offfset = detected_prefix_size + detected_prefix_pad_size
    print("Secret size: ", len(secret_base64)*3/4)
    print("Detected block size: ", detected_block_size)
    print("Detected ECB mode: ", detected_ecb_mode)
    print("Detected prefix size: ", detected_prefix_size)
    print("Detected prefix pad size: ", detected_prefix_pad_size)
    block_num = 0
    deciphered_message = []
    previous_block = [ord('A')]*(detected_block_size+detected_prefix_pad_size)
    decyphering_running = True
    while decyphering_running:
        deciphered_block = []
        pad_size = 1
        while pad_size <= detected_block_size:
            test_block = previous_block[pad_size:]
            ciphered_block = oracle.encrypt(bytearray(test_block))
            test_val = 0x0
            tmp_block = test_block+deciphered_block+[test_val]
            tmp_ciphered_block = oracle.encrypt(bytearray(tmp_block))
            block_offset = block_num*detected_block_size
            oldBlock = ciphered_block[detected_offfset+block_offset:
                                      detected_offfset+block_offset+detected_block_size]
            newBlock = tmp_ciphered_block[detected_offfset:
                                          detected_offfset+detected_block_size]
            while newBlock != oldBlock and test_val < 255:
                test_val = test_val + 1
                tmp_block = test_block+deciphered_block+[test_val]
                tmp_ciphered_block = oracle.encrypt(bytearray(tmp_block))
                newBlock = tmp_ciphered_block[detected_offfset:
                                              detected_offfset+detected_block_size]
            deciphered_block += [test_val]
            pad_size += 1
        previous_block = [ord('A')]*detected_prefix_pad_size + deciphered_block
        deciphered_message += deciphered_block
        block_num += 1
        if block_num >= len(ciphered_block)/detected_block_size:
            decyphering_running = False
    print(bytearray(deciphered_message))

def s2challenge15():
    print("Challenge 15 start")
    test_string1 = (b"ICE ICE BABY\x04\x04\x04\x04")
    test_string2 = (b"ICE ICE BABY\x05\x05\x05\x05")
    test_string3 = (b"ICE ICE BABYABCD\x10\x10\x10\x10\x10"
                    b"\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10")
    test_strings = [test_string1, test_string2, test_string3]
    
    for test_string in test_strings:
        try:
            test_string = stripPKCS7(test_string, 16)
        except:
            print("Test string ", test_string," has invalid padding.")
        else:
            print("Stripped data1: ", test_string)

def s2challenge16():
    print("Challenge 16 start")
    oracle = OracleCbcBitFlipping()
    user_data = "aaaaaaaaaaaaaaaa"
    user_data += ":admin<true:aaaa"
    secret_data = bytearray(oracle.encryptData(user_data))
    secret_data[32] = secret_data[32] ^ 0x1
    secret_data[38] = secret_data[38] ^ 0x1
    secret_data[43] = secret_data[43] ^ 0x1
    print("IsAdmin: ", oracle.isAdmin(secret_data))
