'''
Created on Oct 6, 2020

@author: akoltys
'''
from common.utils import bytes_from_base64, bytes_xor
from Crypto.Cipher import AES
from common.oracles import OracleRandomAccessRWCtr, OracleCtrBitFlipping,\
    OracleCbcKeyIsIV, OracleSha1LenghtExtension, OracleMD4LenghtExtension,\
    generateRandomDataSize
from common.hash import custom_sha1, custom_md4, custom_hmac
from common.httpsrv import HttpSrvCustom, RequestHandlerHMAC50ms

import hashlib
import struct
import urllib.request
import time

def s4challenge25():
    print("Challenge 25")
    with open('testfiles/25.txt') as f:
        plain_data = f.read().replace('\n', '')
    plain_data = bytes_from_base64(plain_data)
    cipher = AES.new(b'YELLOW SUBMARINE', AES.MODE_ECB)
    plain_data = cipher.decrypt(plain_data)
    
    oracle = OracleRandomAccessRWCtr(plain_data)
    print(oracle.getPlain())
    secret = b''+oracle.getSecret()
    print(secret)
    oracle.updateSecret(bytearray([0]*len(secret)), 0)
    tmp_secret = oracle.getSecret()
    print(bytes_xor(secret, tmp_secret))

def s4challenge26():
    print("Challenge 26")
    oracle = OracleCtrBitFlipping()
    user_data = "aaaaaaaaaaaaaaaa"
    user_data += ":admin<true:aaaa"
    secret_data = bytearray(oracle.encryptData(user_data))
    secret_data[48] = secret_data[48] ^ 0x1
    secret_data[54] = secret_data[54] ^ 0x1
    secret_data[59] = secret_data[59] ^ 0x1
    print("IsAdmin: ", oracle.isAdmin(secret_data))

def s4challenge27():
    print("Challenge 27")
    oracle = OracleCbcKeyIsIV()
    secret_data = oracle.encryptData("A"*32)
    secret_data = secret_data[:16]+bytearray([0]*16)+secret_data[:16]
    try:
        isAdmin = oracle.isAdmin(secret_data)
        if isAdmin:
            print("User is admin")
        else:
            print("User is not admin")
    except Exception as e:
        invalid_plain = bytes.fromhex(str(e))
        print("Data: >", invalid_plain)
        print("Data: >", invalid_plain[:16])
        print("Data: >", invalid_plain[32:48])
        enc_key = bytes_xor(invalid_plain[:16], invalid_plain[32:48])
        print("Recovered key: ", enc_key)
        print("Oracle key: ", oracle.getKey())

def s4challenge28():
    print("Challenge 28")
    test_message = b'Hello, World'
    tmp_mac1 = custom_sha1(b'secret_key1'+test_message)
    tmp_mac2 = custom_sha1(b'secret_key2'+test_message)
    tmp_mac3 = custom_sha1(b'secret_key3'+test_message)
    tmp_mac4 = custom_sha1(b'secret_key4'+test_message)
    print("Hash1: ", tmp_mac1.hex())
    print("Hash2: ", tmp_mac2.hex())
    print("Hash3: ", tmp_mac3.hex())
    print("Hash4: ", tmp_mac4.hex())

def s4challenge29():
    print("Challenge 29")
    oracle = OracleSha1LenghtExtension()
    message = b"comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon"
    additional_message = b";admin=true"
    msg_mac = oracle.generateMAC(message)
    print("Original message MAC: ", msg_mac.hex())
    for key_len in range(1, 100):
        tampered_message = b"A"*key_len + message
        msg_len = len(tampered_message)
        tampered_message = tampered_message + bytearray([0x80])
        # must append number of 0's that msg+0's+64-bit msg size is multiple of 64  
        pad_len = (56-len(tampered_message))%64
        tampered_message = tampered_message + bytearray([0] * pad_len)
        # append bit size of the message
        tampered_message = tampered_message + struct.pack(">Q", msg_len*8)
        offset_len = len(tampered_message)
        tampered_message = tampered_message + additional_message

        tmp_mac = custom_sha1(tampered_message, offset_len, msg_mac)
        if oracle.validateMessage(tampered_message[key_len:], tmp_mac):
            print("Guesssed secret key length: ", pad_len)
            return
    print("Failed to tamper message")

def s4challenge30():
    print("Challenge 30")
    oracle = OracleMD4LenghtExtension()
    message = b"comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon"
    
    additional_message = b";admin=true"
    msg_mac = oracle.generateMAC(message)
    print("Original message MAC: ", msg_mac.hex())
    for key_len in range(1, 100):
        tampered_message = b"A"*key_len + message
        msg_len = len(tampered_message)
        tampered_message = tampered_message + bytearray([0x80])
        # must append number of 0's that msg+0's+64-bit msg size is multiple of 64  
        pad_len = (56-len(tampered_message))%64
        tampered_message = tampered_message + bytearray([0] * pad_len)
        # append bit size of the message
        tampered_message = tampered_message + struct.pack("<Q", msg_len*8)
        offset_len = len(tampered_message)
        tampered_message = tampered_message + additional_message

        tmp_mac = custom_md4(tampered_message, offset_len, msg_mac)
        if oracle.validateMessage(tampered_message[key_len:], tmp_mac):
            print("Guesssed secret key length: ", pad_len)
            return
    print("Failed to tamper message")

def s4challenge31():
    print("Challenge 31")
    srv_config = {}
    srv_config['hash_key'] = generateRandomDataSize(1, 40)
    srv_config['hash_fun'] = custom_sha1
    srv_config['hash_len'] = 20
    srv_config['hash_blocklen'] = 64
    http_srv = HttpSrvCustom(RequestHandlerHMAC50ms, srv_config)
    http_srv.run()
    tmp_hash = bytearray([0]*20)
    part_url = 'http://192.168.0.32:8000/test?file=foo&signature='
    valid_data = False
    for i in range(20):
        if valid_data == True:
            break
        valid_byte = False
        while not valid_byte:
            if valid_data == True:
                break
            for j in range(256):
                current_timediff = 0.0
                valid_cntr = 0
                for cnt in range(10):
                    time.sleep(0.005)
                    tmp_hash[i] = j
                    full_url = part_url + tmp_hash.hex()
                    start_time = time.perf_counter()
                    try:
                        data = urllib.request.urlopen(full_url)
                        valid_data = True
                        break
                    except:
                        data = "Invalid"
                    stop_time = time.perf_counter()
                    tmp_diff = (stop_time-start_time)
                    print("\n\n\n\nSingle timediff: ", tmp_diff, " Should be:", (i+1)*0.005)
                    if (tmp_diff < (i+1)*0.0055+0.002):
                        current_timediff += tmp_diff
                        valid_cntr += 1
                
                if valid_data == True:
                    break
                if current_timediff-i*0.0055*valid_cntr > 0.00495*valid_cntr:
                    valid_byte = True
                    break
    full_url = part_url + tmp_hash.hex()
    data = urllib.request.urlopen(full_url)
    print("Data: ", data)
    print("Done")

