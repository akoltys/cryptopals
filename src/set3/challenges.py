'''
Created on 16 lip 2020

@author: akoltys
'''
from common.oracles import OracleCbcPadding, OracleCtrRepeatingNonce,\
    OracleMT19937
from common.utils import bytes_from_base64, find_best_key_bytes, bytes_xor_key
from common.aes import CustomAES
from ctypes import c_uint64
from common import mtrng
from time import sleep
from random import randint
from datetime import datetime

def s3challenge17():
    print("Challenge 17")
    oracle = OracleCbcPadding()
    secret = oracle.getCookie()
    decSecret = []
    print("Secret: ", secret)
    while (len(secret) >= 32):
        tmpSecret = bytearray(secret)
        decChunk = []
        for padSize in range(1, 17):
            tmpChunk = secret[-32:-16]
            tmpVal = secret[-16-padSize]
            for padVal in range(0, 256):
                if (padVal != secret[-16-padSize]):
                    tmpChunk[-padSize] = padVal
                    for idx in range(0, len(decChunk)):
                        tmpChunk[-padSize+1+idx] = decChunk[idx]^padSize ^\
                                                   secret[-padSize+idx+1-16]
                    tmpSecret = bytearray([0*x for x in range(0,16)]) + \
                                secret[:-32]+tmpChunk+secret[-16:]
                    if (oracle.consumeCookie(tmpSecret) == True):
                        tmpVal = padVal
                        break
            orgVal = secret[-16-padSize]
            decVal = tmpVal ^ orgVal ^ padSize
            decChunk = [decVal] + decChunk

        decSecret = decChunk + decSecret
        secret = secret[:-16]
    print("Deciphered secret: ", [chr(x) for x in decSecret])
    print(bytearray(decSecret))

def s3challenge18():
    print("Challenge 18")
    secret = ("L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXz"
              "hPweyyMTJULu/6/kXX0KSvoOLSFQ==")
    secret = bytes_from_base64(secret)
    cipher = CustomAES()
    plain = cipher.decode(secret, "YELLOW SUBMARINE", CustomAES.Mode.CTR,
                          c_uint64(0))
    print("Plain data: ", plain)
    print("Secret len: ", len(secret), " Plain len: ", len(plain))

def s3challenge19():
    print("Challenge 19")
    oracle = OracleCtrRepeatingNonce()
    secrets = oracle.getSecrets()
    for secret in secrets:
        print([x for x in secret])
    secret_key = b''
    print(len(max(secrets, key=len)))
    for idx in range(0, len(max(secrets, key=len))):
        tmp = b''
        for secret in secrets:
            if (idx < len(secret)):
                tmp = tmp + bytes([secret[idx]])
        print(idx, len(tmp), tmp)
        key_byte, key_score = find_best_key_bytes(tmp)
        print(key_byte, key_score)
        secret_key += bytearray([key_byte])
    for secret in secrets:
        print(bytearray(bytes_xor_key(secret, secret_key)[:len(secret)]))

def s3challenge20():
    print("Challenge 20")
    with open('testfiles/20.txt') as f:
        secret_data = f.readlines()
        secret_data = [secret[:-1] for secret in secret_data]
        print(secret_data)
        oracle = OracleCtrRepeatingNonce(secret_data)
        secrets = oracle.getSecrets()
        for secret in secrets:
            print([x for x in secret])
        secret_key = b''
        print(len(max(secrets, key=len)))
        for idx in range(0, len(max(secrets, key=len))):
            tmp = b''
            for secret in secrets:
                if (idx < len(secret)):
                    tmp = tmp + bytes([secret[idx]])
            print(idx, len(tmp), tmp)
            key_byte, key_score = find_best_key_bytes(tmp)
            print(key_byte, key_score)
            secret_key += bytearray([key_byte])
        for secret in secrets:
            print(bytearray(bytes_xor_key(secret, secret_key)[:len(secret)]))

def s3challenge21():
    print("Challenge 21")
    rand_gen = mtrng.MTRng(0)
    for i in range(10):
        print(i, '\t', rand_gen.extractNumber())

def s3challenge22():
    print("Challenge 22")
    sleep(randint(1, 40))
    print("First rand sleep finished")
    orig_randgen = mtrng.MTRng(int(datetime.now().timestamp()))
    sleep(randint(1, 40))
    print("Second rand sleep finished")
    first_rand = orig_randgen.extractNumber()
    current_time = int(datetime.now().timestamp())
    for i in range(200):
        clone_randgen = mtrng.MTRng(current_time-i)
        tmp_rand = clone_randgen.extractNumber()
        if tmp_rand == first_rand:
            print("Original seed was: ", (current_time-i))
            return
    print("Could not find original seed")

def s3challenge23():
    print("Challenge 23")
    orig_randgen = mtrng.MTRng(int(datetime.now().timestamp()))
    orig_states = [0] * 624
    for i in range(624):
        orig_states[i] = mtrng.untemperNumber(orig_randgen.extractNumber())
    
    clone_randgen = mtrng.MTRng(0)
    clone_randgen.updateState(orig_states)
    orig_val = orig_randgen.extractNumber()
    clone_val = clone_randgen.extractNumber()
    if (orig_val == clone_val):
        print("Successfully cloned random number generator")
    else:
        print("Failed to clone random number generator")

def s3challenge24():
    print("Challenge 24")
    oracle = OracleMT19937()
    payload = b'AAAAAAAAAAAAAAAAAA'
    enc_data = oracle.encrypt(b'AAAAAAAAAAAAAAAAAA')
    for i in range(0, 0x80000):
        tmprng = mtrng.MTRng(i)
        tmp = bytearray([(x^tmprng.extractNumber())&0xff for x in enc_data])
        if tmp[-len(payload):] == payload:
            print('Seed: ', i)
            break
    print('Original seed: ', oracle.M_SEED)
    
    for idx in range(0, 100):
        sample = oracle.getToken()
        tmp_timestamp = int(datetime.now().timestamp())
        used_timestamp = False
        for seed in range(0, 20):
            tmprng = mtrng.MTRng(tmp_timestamp-seed)
            tmp = bytearray([tmprng.extractNumber()&0xff for _ in range(0, 16)])
            if tmp == sample[1]:
                used_timestamp = True
                break
        print(idx, '.\t: FromTimestamp: ',sample[0],
              '  \tGuess: ',used_timestamp, '   \tPayload: ', sample[1])
