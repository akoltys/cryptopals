'''
Created on 15 maj 2020

@author: akoltys
@brief: this is implementation was created for educational purposes
        and should not be used in any production code
'''
from enum import Enum
from common.utils import bytes_xor, addPaddingPKCS7
from ctypes import c_uint64

class CustomAES(object):
    '''
    classdocs
    '''
    S_BOX = (
        0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
        0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
        0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
        0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
        0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
        0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
        0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
        0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
        0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
        0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
        0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
        0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
        0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
        0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
        0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
        0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
    )
    
    INVS_BOX = (
        0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
        0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
        0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
        0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
        0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
        0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
        0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
        0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
        0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
        0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
        0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
        0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
        0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
        0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
        0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
        0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
    )
    
    R_CON = (
        0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40,
        0x80, 0x1B, 0x36
    )
    
    NK = 16
    NB = 16
    NR = 10
    
    M_NONCE = c_uint64(0)
    M_CNTR = c_uint64(0)
    M_ENONCE = c_uint64(0)
    M_ECNTR = c_uint64(0)
    M_DNONCE = c_uint64(0)
    M_DCNTR = c_uint64(0) 
    
    class Mode(Enum):
        ECB = 0
        CBC = 1
        CTR = 2

    def __init__(self):
        '''
        Constructor
        '''

    def get_blocks(self, data):
        return [bytearray(data[i:i+16]) for i in range(0, len(data), 16)]

    def rot_word(self, w):
        return [w[1], w[2], w[3], w[0]]

    def key_expansion(self, key):
        nk = self.NK
        nb = self.NB
        nr = self.NR
        w = [0]*nb*(nr+1)
        for i in range(0, nk):
            w[i] = key[i]
        
        for i in range(nk, nb*(nr+1), 4):
            temp = w[i-4:i]
            if i%nk == 0:
                temp = self.sub_bytes(self.rot_word(temp))
                idx = int(i/nk)
                temp[0] = temp[0] ^ self.R_CON[idx]
            elif nk > 6 and i%nk == 0:
                temp = self.sub_bytes(temp)
            w[i] = w[i-nk] ^ temp[0]
            w[i+1] = w[i-nk+1] ^ temp[1]
            w[i+2] = w[i-nk+2] ^ temp[2]
            w[i+3] = w[i-nk+3] ^ temp[3]
        
        return w

    def add_round_key(self, s, w):
        for i in range(0, len(s)):
            s[i] = s[i] ^ w[i]
        return s
    
    def sub_bytes(self, s):
        for i in range(0, len(s)):
            s[i] = self.S_BOX[s[i]]
        return s
    
    def inv_sub_bytes(self, s):
        for i in range(0, len(s)):
            s[i] = self.INVS_BOX[s[i]]
        return s
    
    def shift_rows(self, s):
        s[0], s[4], s[8] , s[12] = s[0] , s[4] , s[8] , s[12] 
        s[1], s[5], s[9] , s[13] = s[5] , s[9] , s[13], s[1] 
        s[2], s[6], s[10], s[14] = s[10], s[14], s[2] , s[6] 
        s[3], s[7], s[11], s[15] = s[15], s[3] , s[7] , s[11]
        return s

    def inv_shift_rows(self, s):
        s[0], s[4], s[8] , s[12] = s[0] , s[4] , s[8] , s[12] 
        s[1], s[5], s[9] , s[13] = s[13], s[1] , s[5] , s[9] 
        s[2], s[6], s[10], s[14] = s[10], s[14], s[2] , s[6] 
        s[3], s[7], s[11], s[15] = s[7] , s[11], s[15], s[3]
        return s

    def galois_mul(self, a, b):
        result = 0x0
        for _ in range(0, 8):
            if b&0x1 != 0:
                result = result ^ a
            
            hbit = (a&0x80 != 0)
            a = (a<<1)&0xff
            if (hbit):
                a = a ^ 0x1B
            b = (b>>1)&0xff
        return (result&0xff)

    def mix_column(self, r0, r1, r2, r3):
        a = [r0, r1, r2, r3]
        r0 = (self.galois_mul(2, a[0]) ^ self.galois_mul(3, a[1]) ^ a[2] ^ a[3])&0xFF
        r1 = (self.galois_mul(2, a[1]) ^ self.galois_mul(3, a[2]) ^ a[3] ^ a[0])&0xFF
        r2 = (self.galois_mul(2, a[2]) ^ self.galois_mul(3, a[3]) ^ a[0] ^ a[1])&0xFF
        r3 = (self.galois_mul(2, a[3]) ^ self.galois_mul(3, a[0]) ^ a[1] ^ a[2])&0xFF
        return r0, r1, r2, r3
    
    def mix_columns(self, s):
        s[0] , s[1] , s[2] , s[3]  = self.mix_column(s[0] , s[1] , s[2] , s[3])
        s[4] , s[5] , s[6] , s[7]  = self.mix_column(s[4] , s[5] , s[6] , s[7])
        s[8] , s[9] , s[10], s[11] = self.mix_column(s[8] , s[9] , s[10], s[11])
        s[12], s[13], s[14], s[15] = self.mix_column(s[12], s[13], s[14], s[15])
        return s
    
    def inv_mix_column(self, r0, r1, r2, r3):
        a = [r0, r1, r2, r3]
        r0 = (self.galois_mul(14, a[0]) ^ self.galois_mul(11, a[1]) ^ self.galois_mul(13, a[2]) ^ self.galois_mul(9, a[3]))&0xFF
        r1 = (self.galois_mul(14, a[1]) ^ self.galois_mul(11, a[2]) ^ self.galois_mul(13, a[3]) ^ self.galois_mul(9, a[0]))&0xFF
        r2 = (self.galois_mul(14, a[2]) ^ self.galois_mul(11, a[3]) ^ self.galois_mul(13, a[0]) ^ self.galois_mul(9, a[1]))&0xFF
        r3 = (self.galois_mul(14, a[3]) ^ self.galois_mul(11, a[0]) ^ self.galois_mul(13, a[1]) ^ self.galois_mul(9, a[2]))&0xFF
        return r0, r1, r2, r3    
    
    def inv_mix_coluns(self, s):
        s[0] , s[1] , s[2] , s[3]  = self.inv_mix_column(s[0] , s[1] , s[2] , s[3])
        s[4] , s[5] , s[6] , s[7]  = self.inv_mix_column(s[4] , s[5] , s[6] , s[7])
        s[8] , s[9] , s[10], s[11] = self.inv_mix_column(s[8] , s[9] , s[10], s[11])
        s[12], s[13], s[14], s[15] = self.inv_mix_column(s[12], s[13], s[14], s[15])
        return s
    
    def encode_block(self, data, w):
        if (len(data) != 16):
            return None
        state = data
        nb = self.NB
        nr = self.NR
        
        state = self.add_round_key(state, w[0:nb])
        
        for round_nbr in range(1, nr):
            state = self.sub_bytes(state)
            state = self.shift_rows(state)
            state = self.mix_columns(state)
            state = self.add_round_key(state, w[round_nbr*nb:(round_nbr+1)*nb])
        
        state = self.sub_bytes(state)
        state = self.shift_rows(state)
        state = self.add_round_key(state, w[nr*nb:(nr+1)*nb])
        
        return state
    
    def decode_block(self, data, w):
        if (len(data) != 16):
            return None
        state = data
        nb = self.NB
        nr = self.NR
        
        state = self.add_round_key(state, w[nr*nb:(nr+1)*nb])
        for round_nbr in range(nr-1, 0, -1):
            state = self.inv_shift_rows(state)
            state = self.inv_sub_bytes(state)
            state = self.add_round_key(state, w[round_nbr*nb:(round_nbr+1)*nb])
            state = self.inv_mix_coluns(state)
        
        state = self.inv_shift_rows(state)
        state = self.inv_sub_bytes(state)
        state = self.add_round_key(state, w[0:nb])
        
        return state
    
    def encode_ecb(self, data, key):
        result = []
        data_chunks = self.get_blocks(data)
        key_expanded = self.key_expansion(key)
        for data_chunk in data_chunks:
            result += self.encode_block(data_chunk, key_expanded)
        return bytearray(result)
    
    def encode_cbc(self, data, key, iv):
        result = []
        data_chunks = self.get_blocks(data)
        key_expanded = self.key_expansion(key)
        for data_chunk in data_chunks:
            iv = self.encode_block(bytearray(bytes_xor(data_chunk, iv)), key_expanded)
            result += iv
        return bytearray(result)
    
    def encode_ctr(self, data, key, iv, cnt = 0):
        if iv != None and type(iv) != c_uint64:
            return None
        if iv != None:
            self.M_ENONCE = iv
            self.M_ECNTR = c_uint64(cnt)
        self.M_NONCE = self.M_ENONCE
        self.M_CNTR = self.M_ECNTR
        result = []
        data_chunks = self.get_blocks(data)
        key_expanded = self.key_expansion(key)
        for data_chunk in data_chunks:
            tmpCtr = bytearray(self.M_NONCE) + bytearray(self.M_CNTR)
            tmpResultChunk = self.encode_block(tmpCtr, key_expanded)
            result += bytearray(bytes_xor(tmpResultChunk, data_chunk))
            self.M_CNTR.value += 1
            if self.M_CNTR.value == 0:
                self.M_NONCE.value += 1
        self.M_ENONCE = self.M_NONCE
        self.M_ECNTR = self.M_CNTR
        return bytearray(result)
    
    def encode(self, data, key, mode = Mode.ECB, iv = None, cnt = None):
        if (isinstance(data, str)):
            data = bytearray(data, 'ascii')
        if (isinstance(key, str)):
            key = bytearray(key, 'ascii')
        if mode != self.Mode.CTR:
            data = addPaddingPKCS7(data, 16)
        if mode == self.Mode.ECB:
            return self.encode_ecb(data, key)
        if mode == self.Mode.CBC:
            return self.encode_cbc(data, key, iv)
        if mode == self.Mode.CTR:
            return self.encode_ctr(data, key, iv, cnt if cnt != None else 0)

    def decode_ecb(self, data, key):
        result = []
        data_chunks = self.get_blocks(data)
        key_expanded = self.key_expansion(key)
        for data_chunk in data_chunks:
            result += self.decode_block(data_chunk, key_expanded)
        return bytearray(result).decode('ascii')
    
    def decode_cbc(self, data, key, iv):
        result = []
        data_chunks = self.get_blocks(data)
        key_expanded = self.key_expansion(key)
        for data_chunk in data_chunks:
            next_iv = list(data_chunk)
            result += bytes_xor(self.decode_block(data_chunk, key_expanded), iv)
            iv = next_iv
        return bytearray(result).decode('ascii')
    
    def decode_ctr(self, data, key, iv, cnt = 0):
        if iv != None and type(iv) != c_uint64:
            return None
        if iv != None:
            self.M_DNONCE = iv
            self.M_DCNTR = c_uint64(cnt)
        self.M_NONCE = self.M_DNONCE
        self.M_CNTR = self.M_DCNTR
        result = []
        data_chunks = self.get_blocks(data)
        key_expanded = self.key_expansion(key)
        for data_chunk in data_chunks:
            tmpCtr = bytearray(self.M_NONCE) + bytearray(self.M_CNTR)
            tmpResultChunk = self.encode_block(tmpCtr, key_expanded)
            result += bytearray(bytes_xor(tmpResultChunk, data_chunk))
            self.M_CNTR.value += 1
            if self.M_CNTR.value == 0:
                self.M_NONCE.value += 1
        self.M_DNONCE = self.M_NONCE
        self.M_DCNTR = self.M_CNTR
        return bytearray(result)
    
    def decode(self, data, key, mode = Mode.ECB, iv = None, cnt = None):
        if (isinstance(data, str)):
            data = bytearray(data, 'ascii')
        if (isinstance(key, str)):
            key = bytearray(key, 'ascii')

        if mode == self.Mode.ECB:
            return self.decode_ecb(data, key)
        if mode == self.Mode.CBC:
            return self.decode_cbc(data, key, iv)
        if mode == self.Mode.CTR:
            return self.decode_ctr(data, key, iv, cnt if cnt != None else 0)
