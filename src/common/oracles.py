'''
Created on 28 maj 2020

@author: akoltys
'''
from random import choice, randint
from common.aes import CustomAES
from common.utils import bytes_from_base64, addPaddingPKCS7, urlParamsToJson,\
    stripPKCS7
from Crypto.Cipher import AES
from ctypes import c_uint64
from common import mtrng
from datetime import datetime
import hashlib
from hashlib import sha1
from common.hash import custom_sha1

def generateRandomData(data_size):
    result = []
    for _ in range(0, data_size):
        result.append(randint(0, 255))
    return bytearray(result)

def generateRandomDataSize(min_length, max_length):
    random_length = randint(min_length, max_length)
    return generateRandomData(random_length)

def encryptionOracle(input_data):
    if isinstance(input_data, str):
        input_data = bytearray(input_data, 'ascii')
    input_data = generateRandomDataSize(5, 10) + input_data + generateRandomDataSize(5, 10)
    iv_data = generateRandomData(16)
    key_data = generateRandomData(16)
    ciph = CustomAES()
    ciph_mode = CustomAES.Mode.CBC
    if randint(0, 1) == 0:
        ciph_mode = CustomAES.Mode.ECB
    else:
        ciph_mode = CustomAES.Mode.CBC
    return ciph.encode(input_data, key_data, ciph_mode, iv_data), ciph_mode

class OracleAesECB(object):
    M_ENCRYPTION_KEY = b''
    M_CIPHER = None
    M_TARGET = b''
    
    def __init__(self, payload):
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_TARGET = bytes_from_base64(payload)
        
    def encrypt(self, input_data):
        self.M_CIPHER = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_ECB)
        plain = input_data + self.M_TARGET
        plain = addPaddingPKCS7(plain, 16)
        return self.M_CIPHER.encrypt(plain)

class OracleAesECBHarder(object):
    M_ENCRYPTION_KEY = b''
    M_CIPHER = None
    M_PAYLOAD = b''
    M_TARGET = b''
    
    def __init__(self, payload):
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_PAYLOAD = generateRandomData(randint(1, 15))
        self.M_TARGET = bytes_from_base64(payload)
        
    def encrypt(self, input_data):
        self.M_CIPHER = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_ECB)
        plain = self.M_PAYLOAD + input_data + self.M_TARGET
        plain = addPaddingPKCS7(plain, 16)
        return self.M_CIPHER.encrypt(plain)

class OracleProfileFor(object):
    M_ENCRYPTION_KEY = b''
    
    def __init__(self):
        self.M_ENCRYPTION_KEY = generateRandomData(16)
    
    def getProfile(self, user_mail):
        user_mail = user_mail.replace('&', '').replace('=', '')
        tmpProfile = 'email='+user_mail+'&uid=10&role=user'
        tmpProfile = addPaddingPKCS7(bytearray(tmpProfile, 'ascii'), 16)
        
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_ECB)
        return cipher.encrypt(tmpProfile)
    
    def decodeProfile(self, profile_data):
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_ECB)
        plain = cipher.decrypt(profile_data)
        return urlParamsToJson(plain.decode('ascii'))

class OracleCbcBitFlipping(object):
    M_ENCRYPTION_KEY = b''
    M_IV = b''
    
    def __init__(self):
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_IV = generateRandomData(16)
    
    def encryptData(self, user_data):
        user_data = user_data.replace(';', '')
        user_data = user_data.replace('=', '')
        plain_data = ("comment1=cooking%20MCs;userdata="+
                      user_data+
                      ";comment2=%20like%20a%20pound%20of%20bacon")
        plain_data = addPaddingPKCS7(bytearray(plain_data, 'ascii'), 16)
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_CBC, self.M_IV)
        return cipher.encrypt(plain_data)

    def isAdmin(self, ciphered_data):
        result = False
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_CBC, self.M_IV)
        plain_data = cipher.decrypt(ciphered_data)
        print(plain_data)
        if b";admin=true;" in plain_data:
            result = True
        return result

class OracleCbcPadding(object):
    M_SECRETS = [
        (b'MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc='),
        (b'MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic='),
        (b'MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw=='),
        (b'MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg=='),
        (b'MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl'),
        (b'MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA=='),
        (b'MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw=='),
        (b'MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8='),
        (b'MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g='),
        (b'MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93')
    ]
    
    M_SECRET = None
    M_ENCRYPTION_KEY = b''
    
    def __init__(self):
        self.M_SECRET = addPaddingPKCS7(choice(self.M_SECRETS), 16)
        self.M_ENCRYPTION_KEY = generateRandomData(16)
    
    def getCookie(self):
        iv = generateRandomData(16)
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_CBC, iv)
        return bytearray(iv) + bytearray(cipher.encrypt(self.M_SECRET))
    
    def consumeCookie(self, cookie):
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_CBC, 
                         cookie[:16])
        data = cipher.decrypt(cookie[16:])
        try:
            stripPKCS7(data, 16)
        except:
            return False
        else:
            return True

class OracleCtrRepeatingNonce(object):
    
    M_SECRETS = [
        b'SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ==',
        b'Q29taW5nIHdpdGggdml2aWQgZmFjZXM=',
        b'RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ==',
        b'RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4=',
        b'SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk',
        b'T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==',
        b'T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ=',
        b'UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==',
        b'QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU=',
        b'T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl',
        b'VG8gcGxlYXNlIGEgY29tcGFuaW9u',
        b'QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA==',
        b'QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk=',
        b'QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg==',
        b'QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo=',
        b'QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=',
        b'VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA==',
        b'SW4gaWdub3JhbnQgZ29vZCB3aWxsLA==',
        b'SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA==',
        b'VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg==',
        b'V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw==',
        b'V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA==',
        b'U2hlIHJvZGUgdG8gaGFycmllcnM/',
        b'VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w=',
        b'QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4=',
        b'VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ=',
        b'V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs=',
        b'SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA==',
        b'U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA==',
        b'U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4=',
        b'VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA==',
        b'QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu',
        b'SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc=',
        b'VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs',
        b'WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs=',
        b'SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0',
        b'SW4gdGhlIGNhc3VhbCBjb21lZHk7',
        b'SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw=',
        b'VHJhbnNmb3JtZWQgdXR0ZXJseTo=',
        b'QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4='
    ]
    
    M_ENCRYPTION_KEY = b''
    
    def __init__(self, secrets = None):
        if (secrets == None):
            self.M_SECRETS = [bytearray(bytes_from_base64(x)) for x in self.M_SECRETS]
        else:
            self.M_SECRETS = [bytearray(bytes_from_base64(x)) for x in secrets]
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_NONCE = c_uint64(0)
    
    def getSecrets(self):
        cipher = CustomAES()
        return [cipher.encode(secret, self.M_ENCRYPTION_KEY, CustomAES.Mode.CTR,
                              self.M_NONCE) for secret in self.M_SECRETS]

class OracleMT19937(object):
    M_MTRNG = None
    M_PAYLOAD = None
    M_SEED = None

    def __init__(self):
        self.M_SEED = abs(randint(0, 0xffff))
        self.M_MTRNG = mtrng.MTRng(self.M_SEED)
        self.M_PAYLOAD = generateRandomDataSize(5, 16)

    def encrypt(self, data):
        if type(data) != type(b''):
            return None
        result = self.M_PAYLOAD + data
        result = bytearray([(x^self.M_MTRNG.extractNumber())&0xff for x in result])
        return result
    
    def getToken(self):
        use_current = (randint(0, 1) == 1)
        tmprng = None
        if (use_current):
            tmprng = mtrng.MTRng(int(datetime.now().timestamp()))
        else:
            tmprng = mtrng.MTRng(randint(0, 0xffff))
        result = bytearray([tmprng.extractNumber()&0xff for _ in range(0, 16)])
        return (use_current, result)

class OracleRandomAccessRWCtr(object):
    M_ENCRYPTION_KEY = b''
    M_SECRET = b''
    M_NONCE = b''
    
    def __init__(self, data):
        cipher = CustomAES()
        self.M_NONCE = c_uint64(abs(randint(0, 0xffffffffffffffff)))
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_SECRET = cipher.encode(data, self.M_ENCRYPTION_KEY,
                                      CustomAES.Mode.CTR, self.M_NONCE)
    
    def getSecret(self):
        return self.M_SECRET
    
    def updateSecret(self, value, offset):
        cipher = CustomAES()
        start_blk = int(offset/16)
        num_blk = offset%16 + int(len(value)/16)
        num_blk = num_blk + 1 if ((offset+len(value))%16) > 0 else num_blk
        tmp = self.M_SECRET[start_blk*16:(start_blk+num_blk)*16]
        tmp = cipher.decode(tmp, self.M_ENCRYPTION_KEY,
                            CustomAES.Mode.CTR, self.M_NONCE, start_blk)
        tmp[offset%16:offset%16+len(value)] = value
        tmp = cipher.encode(tmp, self.M_ENCRYPTION_KEY,
                            CustomAES.Mode.CTR, self.M_NONCE, start_blk)
        self.M_SECRET[start_blk*16:(start_blk+num_blk)*16] = tmp
    
    def getPlain(self):
        cipher = CustomAES()
        return cipher.decode(self.M_SECRET, self.M_ENCRYPTION_KEY,
                            CustomAES.Mode.CTR, self.M_NONCE)

class OracleCtrBitFlipping(object):
    M_ENCRYPTION_KEY = b''
    M_NONCE = b''
    
    def __init__(self):
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_NONCE = c_uint64(abs(randint(0, 0xffffffffffffffff)))
    
    def encryptData(self, user_data):
        user_data = user_data.replace(';', '')
        user_data = user_data.replace('=', '')
        plain_data = ("comment1=cooking%20MCs;userdata="+
                      user_data+
                      ";comment2=%20like%20a%20pound%20of%20bacon")
        plain_data = addPaddingPKCS7(bytearray(plain_data, 'ascii'), 16)
        cipher = CustomAES()
        return cipher.encode(plain_data, self.M_ENCRYPTION_KEY, 
                             CustomAES.Mode.CTR, self.M_NONCE)

    def isAdmin(self, ciphered_data):
        result = False
        cipher = CustomAES()
        plain_data = cipher.decode(ciphered_data, self.M_ENCRYPTION_KEY,
                                   CustomAES.Mode.CTR, self.M_NONCE)
        print(plain_data)
        if b";admin=true;" in plain_data:
            result = True
        return result

class OracleCbcKeyIsIV(object):
    M_ENCRYPTION_KEY = b''
    M_IV = b''
    
    def __init__(self):
        self.M_ENCRYPTION_KEY = generateRandomData(16)
        self.M_IV = self.M_ENCRYPTION_KEY
    
    def encryptData(self, user_data):
        user_data = user_data.replace(';', '')
        user_data = user_data.replace('=', '')
        plain_data = ("comment1=cooking%20MCs;userdata="+
                      user_data+
                      ";comment2=%20like%20a%20pound%20of%20bacon")
        plain_data = addPaddingPKCS7(bytearray(plain_data, 'ascii'), 16)
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_CBC, self.M_IV)
        return cipher.encrypt(plain_data)

    def isAdmin(self, ciphered_data):
        result = False
        cipher = AES.new(self.M_ENCRYPTION_KEY, AES.MODE_CBC, self.M_IV)
        plain_data = cipher.decrypt(ciphered_data)
        for val in plain_data:
            if val > 0x7f:
                print(b"Exception: >" + plain_data[:16])
                print(b"Exception: >" + plain_data[32:48])
                raise Exception(plain_data.hex())
        if b";admin=true;" in plain_data:
            result = True
        return result
    
    def getKey(self):
        return self.M_ENCRYPTION_KEY

class OracleSha1LenghtExtension(object):
    M_SECRET = b''

    def __init__(self):
        self.M_SECRET = generateRandomDataSize(1, 40)
    
    def generateMAC(self, message):
        return sha1(self.M_SECRET + message).digest()

    def validateMessage(self, message, MAC):
        expected = sha1(self.M_SECRET+message).digest()
        print("Got     : ", MAC.hex())
        print("Expected: ", expected.hex())
        return expected == MAC

class OracleMD4LenghtExtension(object):
    M_SECRET = b''

    def __init__(self):
        self.M_SECRET = generateRandomDataSize(1, 40)
    
    def generateMAC(self, message):
        return hashlib.new('MD4', self.M_SECRET + message).digest()

    def validateMessage(self, message, MAC):
        expected = hashlib.new('MD4', self.M_SECRET+message).digest()
        print("Got     : ", MAC.hex())
        print("Expected: ", expected.hex())
        return expected == MAC
