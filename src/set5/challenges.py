'''
Created on May 13, 2021

@author: akoltys
'''
import random
import socket
import json
import threading
import hashlib
import hmac
from time import sleep
from Crypto.Cipher import AES
from common.oracles import generateRandomData
from common.utils import addPaddingPKCS7, stripPKCS7

def power_mod(b, e, m):
    " Without using builtin function "
    x = 1
    while e > 0:
        b, e, x = (
            b * b % m,
            e // 2,
            b * x % m if e % 2 else x
        )
 
    return x

def generate_private_key(p):
    return random.randrange(1, p) % p

def generate_public_key(g, private, p):
    return power_mod(g, private, p)

def generate_session_key(public, private, p):
    return power_mod(public, private, p)

def generate_sha1(s):
    return hashlib.sha1(hex(s).encode()).digest()

def generate_encrypted_message(msg, key):
    msg = addPaddingPKCS7(msg, 16)
    tmp_iv = generateRandomData(16)
    cipher = AES.new(key[:16], AES.MODE_CBC, tmp_iv)
    ciphered_data = cipher.encrypt(msg)
    ciphered_data = ciphered_data + tmp_iv
    return ciphered_data

def decrypt_received_message(msg, key):
    tmp_iv = msg[-16:]
    cipher = AES.new(key[:16], AES.MODE_CBC, tmp_iv)
    plain = cipher.decrypt(msg[:-16])
    plain = plain[:-plain[-1]]
    return plain

def decrypt_received_message_ext(msg, key):
    tmp_iv = msg[-16:]
    cipher = AES.new(key[:16], AES.MODE_CBC, tmp_iv)
    plain = cipher.decrypt(msg[:-16])
    try:
        plain = stripPKCS7(plain, 16)
    except:
        return b'', False
    return plain, True

def wait_and_recv_data(socket, data_size):
    data = b''
    while len(data) == 0:
        data = socket.recv(data_size)
    return data

def s5challenge33():
    print("Challenge 33")
    p = 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
    g = 2
    a = generate_private_key(p) #random.randrange(1, p) % p
    b = generate_private_key(p) #random.randrange(1, p) % p
    print("a: ", a)
    print("b: ", b)
    A = generate_public_key(g, a, p) #power_mod(g, a, p) #(g**a)%p
    B = generate_public_key(g, b, p) #power_mod(g, b, p)#(g**b)%p
    print("A: ", A)
    print("B: ", B)
    s1 = generate_session_key(B, a, p) #power_mod(B, a, p)#(B**a)%p
    s2 = generate_session_key(A, b, p) #power_mod(A, b, p)#(A**b)%p
    print("S1: ", s1)
    print("S2: ", s2)

def thread_b_function(HOST_B, PORT_B):
    print("Starting thread B at: ", HOST_B, ":", PORT_B)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_b:
        socket_b.bind((HOST_B, PORT_B))
        socket_b.listen()
        conn, addr = socket_b.accept()
        with conn:
            print("Connected: ", addr)
            data = wait_and_recv_data(conn, 4*1024)
            print("Received init session data: ", data)
            json_init_data = json.loads(data)
            private_b = generate_private_key(json_init_data["p"])
            public_b = generate_public_key(json_init_data["g"], private_b, json_init_data["p"])
            key = generate_session_key(json_init_data["a"], private_b, json_init_data["p"])
            session_init_response = json.dumps(
                {
                    "b": public_b
                }
            )
            print("B KEY: ", key)
            key = generate_sha1(key)
            print("B KEY HASH: ", key)
            conn.sendall(str.encode(session_init_response))
            data = wait_and_recv_data(conn, 4*1024)
            plain_data = decrypt_received_message(data, key)
            print("B RECEIVED MSG: ", plain_data)
            conn.sendall(generate_encrypted_message(plain_data, key))

def thread_m_function(HOST_M, PORT_M, HOST_B, PORT_B):
    print("Starting thread M at: ", HOST_M, ":", PORT_M)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_ma:
        socket_ma.bind((HOST_M, PORT_M))
        socket_ma.listen()
        conn, addr = socket_ma.accept()
        with conn:
            print("Connected: ", addr)
            data = wait_and_recv_data(conn, 4*1024)
            json_init_data = json.loads(data)
            json_init_data_forged = json.dumps(
                {
                    "p": json_init_data["p"],
                    "g": json_init_data["g"],
                    "a": json_init_data["p"]
                }
            )
            print("M Sending forged init data to B: ", json_init_data_forged)
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_mb:
                socket_mb.connect((HOST_B, PORT_B))
                socket_mb.sendall(str.encode(json_init_data_forged))
                data = wait_and_recv_data(socket_mb, 4*1024)
                json_init_resp_data = json.loads(data)
                data_forged = json.dumps(
                    {
                        "b": json_init_data["p"]
                    }
                )
                key_a = 0
                key_b = 0
                key_a = generate_sha1(key_a)
                key_b = generate_sha1(key_b)
                print("MA KEY HASH: ", key_a)
                print("MB KEY HASH: ", key_b)
                print("Sending forged init data resp to A")
                conn.sendall(str.encode(data_forged))
                data = wait_and_recv_data(conn, 4*1024)
                plain_data = decrypt_received_message(data, key_a)
                print("MA RECEIVED MSG: ", plain_data)
                data = generate_encrypted_message(plain_data, key_b)
                socket_mb.sendall(data)
                data = wait_and_recv_data(socket_mb, 4*1024)
                plain_data = decrypt_received_message(data, key_b)
                print("MB RECEIVED MSG: ", plain_data)
                data = generate_encrypted_message(plain_data, key_a)
                conn.sendall(data)

def s5challenge34_part1():
    HOST_B  = '127.0.0.1'
    PORT_B = 40002
    public_p = 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
    public_g = 2
    thread_b_handle = threading.Thread(target=thread_b_function, args=(HOST_B, PORT_B))
    thread_b_handle.start()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_a:
        socket_a.connect((HOST_B, PORT_B))
        private_a = generate_private_key(public_p)
        public_a = generate_public_key(public_g, private_a, public_p)
        session_init_data = json.dumps(
            {
                "p": public_p,
                "g": public_g,
                "a": public_a
            }
        )
        socket_a.sendall(str.encode(session_init_data))
        recv_session_init_data = wait_and_recv_data(socket_a, 4*1024)#socket_a.recv(4*1024)
        json_init_data = json.loads(recv_session_init_data)
        key = generate_session_key(json_init_data["b"], private_a, public_p)
        print("A KEY: ", key)
        key = generate_sha1(key)
        print("A KEY HASH: ", key)
        socket_a.sendall(generate_encrypted_message(b'Hello darkness my old friend...', key))
        data = wait_and_recv_data(socket_a, 4*1024)#socket_a.recv(4*1024)
        plain_data = decrypt_received_message(data, key)
        print("A RECEIVED MSG: ", plain_data)
    thread_b_handle.join()

def s5challenge34_part2():
    HOST_B  = '127.0.0.1'
    HOST_M  = '127.0.0.1'
    PORT_B = 40002
    PORT_M = 40001
    public_p = 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
    public_g = 2
    thread_b_handle = threading.Thread(target=thread_b_function, args=(HOST_B, PORT_B))
    thread_m_handle = threading.Thread(target=thread_m_function, args=(HOST_M, PORT_M, HOST_B, PORT_B))
    thread_m_handle.start()
    thread_b_handle.start()
    sleep(1)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_a:
        socket_a.connect((HOST_M, PORT_M))
        private_a = generate_private_key(public_p)
        public_a = generate_public_key(public_g, private_a, public_p)
        session_init_data = json.dumps(
            {
                "p": public_p,
                "g": public_g,
                "a": public_a
            }
        )
        socket_a.sendall(str.encode(session_init_data))
        recv_session_init_data = b''
        while len(recv_session_init_data) == 0:
            recv_session_init_data = socket_a.recv(4*1024)
        json_init_data = json.loads(recv_session_init_data)
        key = generate_session_key(json_init_data["b"], private_a, public_p)
        key = generate_sha1(key)
        print("A KEY HASH: ", key)
        socket_a.sendall(generate_encrypted_message(b'Hello darkness my old friend...', key))
        data = wait_and_recv_data(socket_a, 4*1024)#socket_a.recv(4*1024)
        plain_data = decrypt_received_message(data, key)
        print("A RECEIVED MSG: ", plain_data)
    thread_m_handle.join()
    thread_b_handle.join()

def s5challenge34():
    print("Challenge 34")
    #s5challenge34_part1()
    s5challenge34_part2()

def thread_a35_function(HOST_B, PORT_B):
    public_p = 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
    public_g = 2
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_a:
        socket_a.connect((HOST_B, PORT_B))
        private_a = generate_private_key(public_p)
        public_a = generate_public_key(public_g, private_a, public_p)
        # Send p, g to B
        session_init_data = json.dumps(
            {
                "p": public_p,
                "g": public_g
            }
        )
        socket_a.sendall(str.encode(session_init_data))
        
        # Receive ACK from B
        recv_session_init_data = wait_and_recv_data(socket_a, 4*1024)
        if recv_session_init_data != b'ACK':
            print("ACK not received: ", recv_session_init_data)
            exit(-1)

        # Send public A key to B
        session_init_data = json.dumps(
            {
                "a": public_a
            }
        )
        socket_a.sendall(str.encode(session_init_data))

        # Receive public B key from B
        recv_session_init_data = wait_and_recv_data(socket_a, 4*1024)
        json_init_data = json.loads(recv_session_init_data)

        # Generate session key
        key = generate_session_key(json_init_data["b"], private_a, public_p)
        key = generate_sha1(key)
        print("A KEY HASH: ", key)

        # Send secret message to B
        socket_a.sendall(generate_encrypted_message(b'Hello darkness my old friend...', key))
        
        # Receive secret message from B
        data = wait_and_recv_data(socket_a, 4*1024)
        plain_data = decrypt_received_message(data, key)
        print("A RECEIVED MSG: ", plain_data)

def thread_b35_function(HOST_B, PORT_B):
    print("Starting thread B at: ", HOST_B, ":", PORT_B)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_b:
        socket_b.bind((HOST_B, PORT_B))
        socket_b.listen()
        conn, addr = socket_b.accept()
        with conn:
            # Receive p and g from A
            print("Connected: ", addr)
            data = wait_and_recv_data(conn, 4*1024)
            print("Received init session data: ", data)
            json_init_data = json.loads(data)
            private_b = generate_private_key(json_init_data["p"])
            public_b = generate_public_key(json_init_data["g"], private_b, json_init_data["p"])

            # Send ACK to A
            conn.sendall(b'ACK')

            # Receive public A key from A
            data = wait_and_recv_data(conn, 4*1024)
            json_init_data2 = json.loads(data)

            # Generate public key B session key
            key = generate_session_key(json_init_data2["a"], private_b, json_init_data["p"])
            print("B KEY: ", key)
            key = generate_sha1(key)
            print("B KEY HASH: ", key)

            # Send public key B to A
            session_init_response = json.dumps(
                {
                    "b": public_b
                }
            )
            conn.sendall(str.encode(session_init_response))

            # Receive secret message from A
            data = wait_and_recv_data(conn, 4*1024)
            plain_data = decrypt_received_message(data, key)
            print("B RECEIVED MSG: ", plain_data)

            # Send secret message to A
            conn.sendall(generate_encrypted_message(plain_data, key))

def thread_m35_function(HOST_M, PORT_M, HOST_B, PORT_B, G_VAL):
    print("Starting thread M at: ", HOST_M, ":", PORT_M)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_ma:
        socket_ma.bind((HOST_M, PORT_M))
        socket_ma.listen()
        conn, addr = socket_ma.accept()
        with conn:
            print("Connected: ", addr)
            data = wait_and_recv_data(conn, 4*1024)
            json_init_data = json.loads(data)
            P_VAL = json_init_data["p"]
            json_init_data_forged = json.dumps(
                {
                    "p": json_init_data["p"],
                    "g": G_VAL
                }
            )
            print("M Sending forged init data to B: ", json_init_data_forged)
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_mb:
                socket_mb.connect((HOST_B, PORT_B))
                
                # Send forged init data to B 
                socket_mb.sendall(str.encode(json_init_data_forged))
                
                # Receive ACK from B and forward it to A
                data = wait_and_recv_data(socket_mb, 4*1024)
                conn.sendall(data)

                # Recive public A key from A, forge it and sent to B
                data = wait_and_recv_data(conn, 4*1024)
                json_init_data2 = json.loads(data)
                key_a = 0
                key_b = 0
                if G_VAL == 1:
                    json_init_data2["a"] = 1
                    key_a = 1
                    key_b = 1
                elif G_VAL == P_VAL:
                    json_init_data2["a"] = 0
                    key_a = 0
                    key_b = 0
                else:
                    json_init_data2["a"] = 1
                    key_a = 1
                    key_b = 1
                data = json.dumps(json_init_data2)
                socket_mb.sendall(str.encode(data))

                # Receive public B key and send it to A
                data = wait_and_recv_data(socket_mb, 4*1024)
                key_a = generate_sha1(key_a)
                key_b = generate_sha1(key_b)
                print("MA KEY HASH: ", key_a)
                print("MB KEY HASH: ", key_b)
                conn.sendall(data)

                data = wait_and_recv_data(conn, 4*1024)
                plain_data, valid = decrypt_received_message_ext(data, key_a)
                if not valid:
                    key_a = generate_sha1(P_VAL-1)
                    plain_data, valid = decrypt_received_message_ext(data, key_a)
                print("MA RECEIVED MSG: ", plain_data, valid)
                data = generate_encrypted_message(plain_data, key_b)
                socket_mb.sendall(data)
                data = wait_and_recv_data(socket_mb, 4*1024)
                plain_data, valid = decrypt_received_message_ext(data, key_b)
                print("MB RECEIVED MSG: ", plain_data, valid)
                data = generate_encrypted_message(plain_data, key_a)
                conn.sendall(data)

def s5challenge35_part1():
    print("Challenge 35 par1 (g = 1)")
    HOST_B  = '127.0.0.1'
    HOST_M  = '127.0.0.1'
    PORT_B = 40002
    PORT_M = 40001
    thread_b_handle = threading.Thread(target=thread_b35_function, args=(HOST_B, PORT_B))
    thread_m_handle = threading.Thread(target=thread_m35_function, args=(HOST_M, PORT_M, HOST_B, PORT_B, 1))
    thread_m_handle.start()
    thread_b_handle.start()
    sleep(1)
    thread_a35_function(HOST_M, PORT_M)
    thread_m_handle.join()
    thread_b_handle.join()

def s5challenge35_part2():
    print("Challenge 35 par2 (g = p)")
    P_VAL = 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
    HOST_B  = '127.0.0.1'
    HOST_M  = '127.0.0.1'
    PORT_B = 40002
    PORT_M = 40001
    thread_b_handle = threading.Thread(target=thread_b35_function, args=(HOST_B, PORT_B))
    thread_m_handle = threading.Thread(target=thread_m35_function, args=(HOST_M, PORT_M, HOST_B, PORT_B, P_VAL))
    thread_m_handle.start()
    thread_b_handle.start()
    sleep(1)
    thread_a35_function(HOST_M, PORT_M)
    thread_m_handle.join()
    thread_b_handle.join()


def s5challenge35_part3():
    print("Challenge 35 par3 (g = p-1)")
    print("Challenge 35 par2 (g = p)")
    P_VAL = 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
    HOST_B  = '127.0.0.1'
    HOST_M  = '127.0.0.1'
    PORT_B = 40002
    PORT_M = 40001
    thread_b_handle = threading.Thread(target=thread_b35_function, args=(HOST_B, PORT_B))
    thread_m_handle = threading.Thread(target=thread_m35_function, args=(HOST_M, PORT_M, HOST_B, PORT_B, P_VAL-1))
    thread_m_handle.start()
    thread_b_handle.start()
    sleep(1)
    thread_a35_function(HOST_M, PORT_M)
    thread_m_handle.join()
    thread_b_handle.join()

def s5challenge35():
    print("Challenge 35")
    #s5challenge35_part1()
    #s5challenge35_part2()
    s5challenge35_part3()

def server36(config):
    # Generate salt as random integer
    # Generate string xH=SHA256(salt|password)
    # Convert xH to integer x somehow (put 0x on hexdigest)
    # Generate v=g**x % N
    # Save everything but x, xH
    SALT = random.randrange(1, config['N'])
    data_to_hash = str(SALT)+config['P']
    xH = hashlib.sha256(data_to_hash.encode())
    x = int(xH.hexdigest(), 16)
    v = power_mod(config['g'], x, config['N'])
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_ma:
        socket_ma.bind((config['host'], config['port']))
        socket_ma.listen()
        conn, addr = socket_ma.accept()
        with conn:
            print("Connected: ", addr)
            data = wait_and_recv_data(conn, 4*1024)
            print("Received: ", data)
            b = random.randrange(1, config['N'])
            B = config['k']*v + power_mod(config['g'], b, config['N'])
            json_data = json.loads(data)
            A = json_data['A']
            # Send salt, B=kv + g**b % N
            resp = json.dumps({
                'salt': SALT,
                'B': B
            }).encode()
            conn.sendall(resp)
            sleep(1)
            # Compute string uH = SHA256(A|B), u = integer of uH
            # Generate S = (A * v**u) ** b % N
            # Generate K = SHA256(S)
            uH = hashlib.sha256((str(A)+str(B)).encode())
            u = int(uH.hexdigest(),16)
            S = power_mod(A*power_mod(v, u, config['N']), b, config['N'])
            K = hashlib.sha256(str(S).encode())
            # print('SRV::S: ', S)
            # print('SRV::K: ', K.hexdigest())
            data = wait_and_recv_data(conn, 4*1024)
            hmac_obj = hmac.new(K.digest(), str(SALT).encode(), hashlib.sha256)
            valid_hmac = hmac_obj.digest()
            if (valid_hmac == data):
                conn.sendall(b'OK')
            else:
                conn.sendall(b'FAIL')



def s5challenge36():
    print("Challenge 36")
    config = {
        'N': 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff,
        'g': 2,
        'k': 3,
        'I': 'person@test.com',
        'P': 'superpassword',
        'host': '127.0.0.1',
        'port': 40038
    }
    server_handle = threading.Thread(target=server36, args=(config,))
    server_handle.start()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_a:
        socket_a.connect((config["host"], config["port"]))
        # Send I, A=g**a % N (a la Diffie Hellman)
        a = random.randrange(1, config["N"])
        A = power_mod(config["g"], a, config["N"])
        session_init_data = json.dumps(
            {
                "I": config['I'],
                "A": A
            }
        ).encode()
        socket_a.sendall(session_init_data)
        resp = wait_and_recv_data(socket_a, 4096)
        print("Client received: ", resp)
        json_data = json.loads(resp)
        # Compute string uH = SHA256(A|B), u = integer of uH
        B = json_data['B']
        SALT = json_data['salt']
        uH = hashlib.sha256((str(A)+str(B)).encode())
        u = int(uH.hexdigest(),16)
        # Generate string xH=SHA256(salt|password)
        # Convert xH to integer x somehow (put 0x on hexdigest)
        # Generate S = (B - k * g**x)**(a + u * x) % N
        # Generate K = SHA256(S)
        data_to_hash = str(SALT)+config['P']
        xH = hashlib.sha256(data_to_hash.encode())
        x = int(xH.hexdigest(), 16)
        S = power_mod(B - config['k'] * power_mod(config['g'], x, config['N']), a+u*x, config['N'])
        K = hashlib.sha256(str(S).encode())
        # print("CLIENT::S: ", S)
        # print("CLIENT::K: ", K.hexdigest())
        hmac_obj = hmac.new(K.digest(), str(SALT).encode(), hashlib.sha256)
        socket_a.sendall(hmac_obj.digest())
        resp = wait_and_recv_data(socket_a, 4096)
        print("CLIENT::Srv response: ", resp)

    server_handle.join()

def s5challenge37():
    print("Challenge 37")
    config = {
        'N': 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff,
        'g': 2,
        'k': 3,
        'I': 'person@test.com',
        'P': 'superpassword',
        'host': '127.0.0.1',
        'port': 40038
    }
    server_handle = threading.Thread(target=server36, args=(config,))
    server_handle.start()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_a:
        socket_a.connect((config["host"], config["port"]))
        # Send I, A=g**a % N (a la Diffie Hellman)
        a = random.randrange(1, config["N"])
        A = config["N"] #power_mod(config["g"], a, config["N"])
        session_init_data = json.dumps(
            {
                "I": config['I'],
                "A": A
            }
        ).encode()
        socket_a.sendall(session_init_data)
        resp = wait_and_recv_data(socket_a, 4096)
        print("Client received: ", resp)
        json_data = json.loads(resp)
        # Compute string uH = SHA256(A|B), u = integer of uH
        B = json_data['B']
        SALT = json_data['salt']
        uH = hashlib.sha256((str(A)+str(B)).encode())
        u = int(uH.hexdigest(),16)
        # Generate string xH=SHA256(salt|password)
        # Convert xH to integer x somehow (put 0x on hexdigest)
        # Generate S = (B - k * g**x)**(a + u * x) % N
        # Generate K = SHA256(S)
        data_to_hash = str(SALT)+config['P']
        xH = hashlib.sha256(data_to_hash.encode())
        x = int(xH.hexdigest(), 16)
        S = 0
        K = hashlib.sha256(str(S).encode())
        print("CLIENT::S: ", S)
        print("CLIENT::K: ", K.hexdigest())
        hmac_obj = hmac.new(K.digest(), str(SALT).encode(), hashlib.sha256)
        socket_a.sendall(hmac_obj.digest())
        resp = wait_and_recv_data(socket_a, 4096)
        print("CLIENT::Srv response: ", resp)

    server_handle.join()

def server38(config):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_ma:
        socket_ma.bind((config['host'], config['port']))
        socket_ma.listen()
        conn, addr = socket_ma.accept()
        with conn:
            print("Connected: ", addr)
            data = wait_and_recv_data(conn, 4*1024)
            chars_array = [chr(x) for x in range(ord('a'), ord('z')+1)]
            chars_array = chars_array + [chr(x) for x in range(ord('A'), ord('Z')+1)]
            SALT = random.randrange(1, config['N'])
            b = random.randrange(1, config['N'])
            B = power_mod(config['g'], b, config['N'])
            u = random.randrange(1, config['N'])
            json_data = json.loads(data)
            A = json_data['A']
            resp = json.dumps({
                'salt': SALT,
                'B': B,
                'u': u
            }).encode()
            conn.sendall(resp)
            sleep(1)
            data = wait_and_recv_data(conn, 4*1024)

            forged = []
            got_password = False
            while (got_password == False and len(forged) <=16):
                if len(forged) == 0:
                    forged = [0]
                else:
                    idx = 0
                    while idx < len(forged):
                        forged[idx] = forged[idx]+1
                        if forged[idx] >= len(chars_array):
                            forged[idx] = 0
                            if idx == (len(forged)-1):
                                forged.append(0)
                                break
                        else:
                            break
                        idx = idx + 1
                print("Forged: ", forged)
                tmp_pass = "".join([chars_array[x] for x in forged])
                print("Checking password: ", tmp_pass)
                data_to_hash = str(SALT)+tmp_pass
                xH = hashlib.sha256(data_to_hash.encode())
                x = int(xH.hexdigest(), 16)
                v = power_mod(config['g'], x, config['N'])
                S = power_mod(A*power_mod(v, u, config['N']), b, config['N'])
                K = hashlib.sha256(str(S).encode())
                hmac_obj = hmac.new(K.digest(), str(SALT).encode(), hashlib.sha256)
                tmp_hmac = hmac_obj.digest()
                if tmp_hmac == data:
                    print("Valid password: ", tmp_pass)
                    got_password = True

            data_to_hash = str(SALT)+config['P']
            xH = hashlib.sha256(data_to_hash.encode())
            x = int(xH.hexdigest(), 16)
            v = power_mod(config['g'], x, config['N'])
            S = power_mod(A*power_mod(v, u, config['N']), b, config['N'])
            K = hashlib.sha256(str(S).encode())
            hmac_obj = hmac.new(K.digest(), str(SALT).encode(), hashlib.sha256)
            valid_hmac = hmac_obj.digest()
            if (valid_hmac == data):
                conn.sendall(b'OK')
            else:
                conn.sendall(b'FAIL')

def s5challenge38():
    print("Challenge 38")
    config = {
        'N': 0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff,
        'g': 2,
        'k': 3,
        'I': 'person@test.com',
        'P': 'abc',
        'host': '127.0.0.1',
        'port': 40038
    }
    server_handle = threading.Thread(target=server38, args=(config,))
    server_handle.start()
    sleep(1)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_a:
        socket_a.connect((config["host"], config["port"]))
        # Send I, A=g**a % N (a la Diffie Hellman)
        a = random.randrange(1, config["N"])
        A = power_mod(config["g"], a, config["N"])
        session_init_data = json.dumps(
            {
                "I": config['I'],
                "A": A
            }
        ).encode()
        socket_a.sendall(session_init_data)
        resp = wait_and_recv_data(socket_a, 4096)
        json_data = json.loads(resp)
        B = json_data['B']
        SALT = json_data['salt']
        u = json_data['u']
        print("CLIENT::Pasword: ", config['P'])
        data_to_hash = str(SALT)+config['P']
        print("CLIENT::data_to_hash: ", data_to_hash)
        xH = hashlib.sha256(data_to_hash.encode())
        print("CLIENT::digest: ", xH.hexdigest())
        x = int(xH.hexdigest(), 16)
        S = power_mod(B, a+u*x, config['N'])
        K = hashlib.sha256(str(S).encode())
        hmac_obj = hmac.new(K.digest(), str(SALT).encode(), hashlib.sha256)
        socket_a.sendall(hmac_obj.digest())
        resp = wait_and_recv_data(socket_a, 4096)
        print("CLIENT::Srv response: ", resp)

    server_handle.join()
